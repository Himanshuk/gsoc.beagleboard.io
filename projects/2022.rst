.. _gsoc-2022-projects:

:far:`calendar-days` 2022
##########################

Adding features to simpPRU
**************************

.. youtube:: a_C_wTEzJJo
   :width: 100%
      
| **Summary:** Expanding on GSoC 2021 progress, this project enhances simpPRU—a Python-like language for PRU C. Goals include adding operators, compiler flags documentation, GitHub Actions for testing, return statements, C invocation, improved error handling, and additional test cases. The project concludes with a research paper showcasing simpPRU's necessity, implementation, and practical usage for increased efficiency, benefiting both beginners and experienced PRU developers.

**Contributor:** Krishna Narayanan

**Mentors:** Vedant Paranjape, Archisman Dey, Pratim Ugale

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/programs/2022/projects/atWuie8j
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2022_Proposal/Adding_features_to_simpPRU
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Greybus for Zephyr Updates
*****************************

.. youtube:: GN82Yiq_kRg
   :width: 100%

| **Summary:** The aim of this project is to support all relevant peripherals (UART, PWM, GPIO IRQ) on BeagleConnect Freedom on Greybus for Zephyr. This involves adding platform-specific changes to enable these protocols and interfaces to existing NuttX sources.

**Contributor:** Hashil Bhatt

**Mentors:** Jason Krinder, Vaishnav Achath, Tim Orlong, Deepak Khatri

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/programs/2022/projects/MvtAmVdl
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2022_Proposal/GreybusforZephyrUpdates
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Running Machine Learning Models on Bela 
***************************************

.. youtube:: AHaTD0bTIpU
   :width: 100%
      
| **Summary:** The goal of this project is to improve the tooling surrounding embedded machine learning on the BeagleBone Black(BBB)/Bela to aid its community in experimenting with machine learning applications for their projects. The specific developer tools chosen for this project are an inference benchmarking tool as well as a perf-based profiler developed for the BBB/Bela platform.

**Contributor:** Ezra Pierce

**Mentors:** Giulio Moro, Jack Armitage, Victor Shepardson

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/programs/2022/projects/ky4Dpka9
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2022_Proposal/Running_Machine_Learning_Models_on_Bela
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

BeagleBoard Cape Compability Layer
***********************************

.. youtube:: kVL0rDWA-sY
   :width: 100%

| **Summary:** The main objective is to enhance BeagleBoneAI compatibility with the Robotics Cape by implementing "incomplete symlink" solutions for missing symbolic links (e.g., GPIO, PWM, SPI, CAN). It also involves creating specific device tree overlays for the Robotics Cape and providing demo code and tutorials to address current documentation gaps. Anticipated benefits include improved compatibility, expanded Cape functionality, and enhanced user guidance.

**Contributor:** Kai Yamada

**Mentors:** Jason Kridner, Tim Orling, Deepak Khatri

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/programs/2022/projects/CgX7RtSl
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2022_Proposal/BeagleBone_Cape_add-on_board_compatibility_layer
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Bb-Config Improvements and GPIO Benchmark
******************************************

.. youtube:: V_Euk5uWY1o
   :width: 100%
      
| **Summary:** Comprising two distinct aspects, the project involves Bb-Config Improvement and GPIO Benchmarking. The former focuses on implementing user-friendly features to enhance functionality, while the latter aims to provide valuable data for developers. The GPIO Benchmarking aspect compares latency performance across different methods of GPIO access, aiding developers in choosing the most suitable approach for their needs.

**Contributor:** Seak Jian De

**Mentors:** Vedant Paranjape, Shreyas Atre, Vaishnav Achath

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/programs/2022/projects/2DbiYPlY
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2022_Proposal/bb-config_improvements_%26_GPIO_Benchmarking
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

Building Bela Images Project
*****************************

.. youtube:: JESc32I59TQ
   :width: 100%

| **Summary:** Bela, tailored for artists and creators, offers an open hardware and software platform for sensor-driven sound interactions. The project focuses on enhancing maintainability by integrating Bela Image builder functionalities into the BeagleBoard Image builder repository. This streamlining effort aims to align the two codebases for improved efficiency in development processes by adding the functionalities of the Bela Image builder repo to the BeagleBoard Image builder repo.

**Contributor:** Kurva Prashanth

**Mentors:** Vedant Paranjape, Vaishnav Achath, Giulio Moro

.. grid:: 2 2 2 2

   .. grid-item::

      .. button-link:: https://summerofcode.withgoogle.com/programs/2022/projects/ykkMkxcR
         :color: info
         :shadow:
         :expand:

         :fab:`google;pst-color-light` - GSoC Registry
   
   .. grid-item::

      .. button-link:: https://elinux.org/BeagleBoard/GSoC/2022_Proposal/Building_Bela_Images
         :color: primary
         :shadow:
         :expand:

         :fab:`linux;pst-color-light` - eLinux Proposal

.. tip::

   Checkout eLinux page for GSoC 2022 projects `here <https://elinux.org/BeagleBoard/GSoC/2022_Projects>`_ for more details.